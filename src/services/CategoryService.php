<?php

namespace Drupal\ep_catalog\services;

use Drupal\node\Entity\Node;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provides functions for interacting with categories in Drupal.
 */
class CategoryService {
  private $entityTypeManager;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * Constructor.
   */
  public function __construct(
    EntityTypeManager $type_man,
    ConfigManager $config,
    LoggerChannelFactoryInterface $loggerFactory
  ) {
    $this->entityTypeManager = $type_man;
    $this->logger = $loggerFactory->get('CategoryService');
  }

  /**
   * Creates a category using the passed in values array.
   *
   * Expects the following format for $category:
   *
   * [
   *  'field_category_code' => string,
   *  'field_category_name' => string
   * ]
   *
   * @throws DomainException
   *   On duplicate field_category_code.
   *
   * @return Drupal\node\Entity\Node|null
   *   Returns the created category, or null on failure.
   *
   * @see \Drupal\Tests\ep_catalog\Kernel\CategoryServiceCreateCategoryTest
   */
  public function createCategory(array $category) {
    $code = $category['field_category_code'];
    if ($this->getCategoryByCode($code) != NULL) {
      throw new \DomainException(
        'Trying to add category with duplicate field_category_code (' . $code . ')'
      );
    }

    $node_info = [
      'title' => $category['field_category_name'],
      'type' => 'ep_category',
      'field_child_categories' => [],
      'status' => 0,
    ];

    $category_node_array = array_merge($node_info, $category);

    $result = NULL;
    try {
      $node = Node::create($category_node_array);
      $node->setPublished(TRUE);
      $node->save();
      $result = $node;
    }
    catch (Exception $err) {
      $this->logger->notice($err);
    }
    return $result;
  }

  /**
   * Gets a category by its nid (node id).
   *
   * @see \Drupal\Tests\ep_catalog\Kernel\CategoryServiceGetCategoryTest
   */
  public function getCategory(string $node_id) {
    return $this->entityTypeManager->getStorage('node')->load($node_id);
  }

  /**
   * Gets multiple categoires by ID.
   *
   * @return array
   *   Returns array of categories.
   *
   * @see \Drupal\Tests\ep_catalog\Kernel\CategoryServiceGetCategoriesTest
   */
  public function getCategories(array $node_ids) {
    return $this->entityTypeManager->getStorage('node')->loadMultiple($node_ids);
  }

  /**
   * Gets a category by category code.
   *
   * @see \Drupal\Tests\ep_catalog\Kernel\CategoryServiceGetCategoryByCodeTest
   */
  public function getCategoryByCode(string $code) {
    $cat_arr = $this
      ->entityTypeManager
      ->getStorage('node')
      ->loadByProperties(['field_category_code' => $code]);

    if (count($cat_arr) > 0) {
      return array_values($cat_arr)[0];
    }
    else {
      return NULL;
    }
  }

  /**
   * Adds the given category as a child of the passed-in category.
   *
   * @throws DomainException
   *   If trying to add same child more than once.
   *   If trying to add parent to its self.
   *
   * @return Drupal\node\Entity\Node
   *   Returns the updated parent.
   */
  public function addChild(Node $parent, Node $child) {
    $child_id = $child->id();
    $parent_id = $parent->id();
    $child_arr = $parent->get('field_child_categories')->getValue();

    // TODO: This is hacky. Probably a built-in way to fix it.
    $child_already_assigned = in_array(['target_id' => $child_id], array_values($child_arr));
    if ($child_already_assigned) {
      return $parent;
    }

    if ($child_id == $parent_id) {
      throw new \DomainException('Cannot add category as child to itself.');
    }

    $child_arr = array_merge($child_arr, [$child]);
    $parent->set('field_child_categories', $child_arr);
    $parent->save();
    return $parent;
  }

  /**
   * Adds the given category ID as a child of the passed-in category.
   *
   * @return Drupal\node\Entity\Node
   *   Returns the updated parent.
   */
  public function addChildId(Node $parent, string $child_id) {
    $child = $this->getCategory($child_id);
    if (!($child instanceof Node)) {
      throw new \InvalidArgumentException('Invalid child id specified.');
    }
    return $this->addChild($parent, $child);
  }

  /**
   * Removes the given child from the parent.
   *
   * @return Drupal\node\Entity\Node
   *   Returns the updated parent.
   */
  public function removeChild(Node $parent, Node $child) {
    $child_id = $child->id();
    return $this->removeChildId($parent, $child_id);
  }

  /**
   * Removes the given child ID from the parent.
   *
   * @return Drupal\node\Entity\Node
   *   Returns the updated parent.
   */
  public function removeChildId(Node $parent, string $child_id) {
    $child_val = ['target_id' => $child_id];
    $child_arr = $parent->get('field_child_categories')->getValue();

    while (($key = array_search($child_val, $child_arr)) !== FALSE) {
      unset($child_arr[$key]);
    }

    $parent->set('field_child_categories', $child_arr);
    $parent->save();
    return $parent;
  }

  /**
   * Deletes a category.  Does NOT delete children.
   */
  public function deleteCategory(Node $category) {
    $category->delete();
  }

  /**
   * Deletes a category.  DOES delete children.
   */
  public function deleteCategoryR(Node $category) {
    $child_id_arr = $category->get('field_child_categories')->getValue();

    foreach ($child_id_arr as $child_target) {
      $child_id = $child_target['target_id'];
      $child = $this->getCategory($child_id);
      if ($child) {
        $this->deleteCategoryR($child);
      }
    }

    $category->delete();
  }

}
