<?php

namespace Drupal\ep_catalog\services;

use Drupal\ep_catalog\Importer\ProductImporterInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Imports an EP catalog by crawling its API.
 */
class ApiCrawlerImporter implements ProductImporterInterface {
  use StringTranslationTrait;

  protected $config;

  /**
   * HTTP client for cortex interactions.
   *
   * @var httpClient
   */
  protected $httpClient;

  /**
   * Product service.
   *
   * @var productService
   */
  protected $productService;

  /**
   * Category service.
   *
   * @var categoryService
   */
  protected $categoryService;

  /**
   * Cortex token.
   *
   * @var token
   */
  private $token;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * Constructor.
   */
  public function __construct(
            Client $httpClient,
            ProductService $prod_svc,
            CategoryService $category_svc,
            ConfigFactory $config_factory,
            TranslationInterface $string_translation,
            LoggerChannelFactoryInterface $loggerFactory
        ) {
    $this->config = $config_factory->get('ep_catalog.settings');
    $this->httpClient = $httpClient;
    $this->categoryService = $category_svc;
    $this->productService = $prod_svc;
    $this->stringTranslation = $string_translation;
    $this->logger = $loggerFactory->get('ep_catalog');
  }

  /**
   * I am a doc description.
   *
   * @inheritdoc
   *
   * Clears existing categories and products, recreates empty root category.
   */
  public function setup() {
    // Verify we can reach cortex and are authorized.
    $root_cortex_url = $this->config->get('ep_endpoint') . '/';
    try {
      $this->cortexGet($root_cortex_url);
    }
    catch (ConnectException $e) {
      $msg = $this->t("Failed connecting to Cortex at URL")
        . " ${root_cortex_url}."
        . $this->t(". Check Cortex URL and try again.");

      $this->logger->error($msg);
      drupal_set_message($msg, 'error');
      return FALSE;
    }
    catch (RequestException $e) {
      $response_code = $e->getResponse()->getStatusCode();
      $reason = $e->getResponse()->getReasonPhrase();
      $msg = $this->t("Error connecting to Cortex. Response code") . "${response_code} ${reason}.";
      $this->logger->error($msg);
      drupal_set_message($msg, 'error');
      return FALSE;
    }

    $root_cat_code = $this->getRootCategoryCode();
    $root_cat_name = $this->config->get('ep_scope') . ' Root';

    // Clear existing products and categories.
    $this->productService->clearAllProducts();
    $root_cat = $this->categoryService->getCategoryByCode($root_cat_code);
    if ($root_cat) {
      $this->categoryService->deleteCategoryR($root_cat);
    }

    // Create root category.
    $root_cat = $this->categoryService->createCategory([
      'field_category_name' => $root_cat_name,
      'field_category_code' => $root_cat_code,
    ]);

    return TRUE;
  }

  /**
   * Get an array containing batches for processing.
   *
   * @return array
   *   Array of top-level category links returned from Cortex.
   */
  public function getBatches() {
    $data = $this->fetchTopLevelCategories();
    return $data['links'];
  }

  /**
   * Processes a batch item.
   *
   * @var array batch
   *   Associative array for a category link returned from Cortex.
   */
  public function processBatch($batch) {
    // Fetch root category.
    $data = $this->fetchCategory($batch['href']);
    $this->importCategory($data);
  }

  /**
   * Gets the top-level navigation resource at /navigations/[scope].
   *
   * @return array
   *   Top level categories result returned from cortex.
   */
  protected function fetchTopLevelCategories() {
    $baseUrl = $this->config->get('ep_endpoint');
    $scope = $this->config->get('ep_scope');
    $url = $baseUrl . '/navigations/' . $scope;
    return $this->cortexGet($url);
  }

  /**
   * Gets the category at the given uri (uri does not include base url)
   *
   * Should be called on all element links returned by fetchTopLevelCategories
   * and all child links returned by fetch_category.
   */
  protected function fetchCategory(String $href) {
    $zoom = "child,items,";
    // . 'items:element:definition';.
    $result = $this->cortexGet($href, $zoom);
    $items_href = $result['_items'][0]['self']['href'];

    if ($items_href) {
      $item_data = $this->fetchItems($items_href);
      $result['_items'][0]['_element'] = $item_data['_element'];
    }
    return $result;
  }

  /**
   * Paginates through item pages. Returns items as if one big zoom query.
   */
  private function fetchItems(String $href) {
    $zoom = 'element:code,';
    $result = NULL;

    do {
      $page = $this->cortexGet($href, $zoom);
      $index_of_next = array_search('next', array_column($page['links'], 'rel'));
      $href = is_int($index_of_next)
                ? $page['links'][$index_of_next]['href']
                : NULL;

      if (!$result) {
        $result = $page;
      }
      else {
        $result['_element'] = array_merge_recursive($result['_element'], $page['_element']);
      }
    } while ($href);

    return $result;
  }

  /**
   * Imports a category, given the response from a category call to cortex.
   */
  protected function importCategory($data, String $parent_id = NULL) {
    $category = $this->createCategory($data);
    $this->assignCategoryToParent($category, $parent_id);
    $this->importChildCategories($data, $category);
    $this->importCategoryItems($data, $category);
  }

  /**
   * Creates a category, given data from cortex.
   */
  private function createCategory($data) {
    $category = $this->categoryService->getCategoryByCode($data['name']);
    if ($category) {
      return $category;
    }
    else {
      return $this->categoryService->createCategory(
        [
          'field_category_code' => $data['name'],
          'field_category_name' => $data['display-name'],
        ]
        );
    }
  }

  /**
   * Assigns a category to its parent.
   */
  private function assignCategoryToParent(Node $category, $parent_id) {
    $parent_node = NULL;
    if (!$parent_id) {
      $root_cat_code = $this->getRootCategoryCode();
      $parent_node = $this->categoryService
        ->getCategoryByCode($root_cat_code);
    }
    else {
      $parent_node = $this->categoryService
        ->getCategory($parent_id);
    }
    $this->categoryService->addChild($parent_node, $category);
  }

  /**
   * Imports child categories from response to a cortex Category call.
   */
  private function importChildCategories($data, Node $category) {
    if (isset($data['_child'])) {
      foreach ($data['_child'] as $child) {
        $child_data = $this->fetchCategory($child['self']['href']);
        $this->importCategory($child_data, $category->id());
      }
    }
  }

  /**
   * Imports all items in a category.
   *
   * @var data
   *   Response from a cortex Category call.
   * @var category
   *   Category to import items for.
   */
  private function importCategoryItems($data, Node $category) {
    if (isset($data['_items'])) {
      $elements = $data['_items'][0]['_element'];
      foreach ($elements as $item_details) {
        $this->importCategoryItem($item_details, $category);
      }
    }
  }

  /**
   * Imports an item and adds to specified category. Skips if already present.
   */
  private function importCategoryItem($item_details, Node $category) {
    $code = $item_details['_code'][0]['code'];
    $product = $this->productService->getProductForSku($code);

    if (!$product) {
      try {
        $this->productService->createProduct([
          'field_default_sku' => $code,
          'field_product_code' => $code,
          'field_parent_categories' => [],
        ]);
      }
      catch (\Exception $e) {
        ksm([$code, $item_details, $e]);
        return;
      }
      $product = $this->productService->getProductForSku($code);
    }

    // Add to category.
    $this->productService->assignToCategory($product, $category);
  }

  /**
   * Get a cortex auth token.
   */
  protected function getToken() {
    if (!$this->token) {
      $baseUrl = $this->config->get('ep_endpoint');
      $scope = $this->config->get('ep_scope');
      $username = $this->config->get('username');
      $password = $this->config->get('password');
      $role = strlen($username) == 0 ? 'PUBLIC' : 'REGISTERED';

      $url = rtrim($baseUrl, '/') . '/oauth2/tokens';
      $options = [
        'form_params' => [
          'scope' => $scope,
          'role' => $role,
          'grant_type' => 'password',
          'username' => $username,
          'password' => $password,
        ],
      ];

      $response = $this->httpClient->post($url, $options);
      $content = json_decode($response->getBody()->getContents());
      $this->token = 'Bearer ' . $content->access_token;
    }

    return $this->token;
  }

  /**
   * Helper function for calling Cortex.
   */
  private function cortexGet(String $url, String $zoom = NULL) {
    $headers = [
      'Authorization' => $this->getToken(),
      'Accept' => 'application/json',
    ];

    $complete_url = $url;
    if ($zoom) {
      $complete_url = $complete_url . "?zoom=" . $zoom;
    }

    $request = new Request('GET', $complete_url, $headers);
    $response = $this->httpClient->send($request);
    $contents = $response->getBody()->getContents();
    return json_decode($contents, TRUE);
  }

  /**
   * Helper, so we're not concatenating the strings in 2+ places.
   */
  public function getRootCategoryCode() {
    $scope = $this->config->get('ep_scope');
    $root_cat_code = 'DRUPAL_' . $scope . '_ROOT';
    return $root_cat_code;
  }

}
