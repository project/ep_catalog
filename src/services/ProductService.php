<?php

namespace Drupal\ep_catalog\services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\ep_catalog\product\ProductEntityManager;
use Drupal\ep_catalog\product\ProductEntityMapper;

/**
 * Provides functions for dealing with products in Drupal.
 */
class ProductService {
  protected $productMapper;

  protected $productManager;

  /**
   * Constructor.
   */
  public function __construct(
    EntityTypeManager $type_man,
    ConfigManager $config,
    Connection $database_connection
  ) {
    $this->productMapper = new ProductEntityMapper($database_connection);
    $this->productManager = new ProductEntityManager($type_man);

    $this->configure($config);
  }

  /**
   * Create a product node and its mapping.
   */
  public function createProduct($product) {
    $node_info = [
      'title' => $product['field_default_sku'],
      'type' => 'ep_product',
      'status' => 0,
    ];

    $product_node_array = array_merge($node_info, $product);

    $node = $this->productManager->create($product_node_array);
    $this->productMapper->createMapping($product['field_default_sku'], $node->id());
  }

  /**
   * Gets the product node for a given sku.
   */
  public function getProductForSku($sku) {
    $nid = $this->productMapper->getNid($sku);
    return $this->productManager->getProductById($nid);
  }

  /**
   * Gets the sku for a given nid.
   */
  public function getSkuForNid($nid) {
    return $this->productMapper->getSku($nid);
  }

  /**
   * TODO: Implement.
   */
  protected function configure(ConfigManager $config) {
    // Stub for now.
  }

  /**
   * Deletes a product and its mapping by ID.
   */
  public function deleteProduct($product_nid) {
    $this->productManager->delete($product_nid);
    $this->productMapper->deleteMapping($product_nid);
  }

  /**
   * Clears ALL product nodes AND mappings.
   */
  public function clearAllProducts() {
    $node_ids = $this->productMapper->getAllNids();

    foreach ($node_ids as $nid) {
      $this->productManager->delete($nid->nid);
    }
    $this->productMapper->clearMappings();
  }

  /**
   * Gets all products.
   *
   * @return array|mixed
   *   Array of products.
   */
  public function getAllProducts() {
    $node_ids = $this->productMapper->getAllNids();
    return $this->productManager->getProductById($node_ids);
  }

  /**
   * Searches for a category assigned to the product.
   *
   * @return int|bool
   *   Returns index of category or FALSE if not found.
   */
  public function getCategoryIndex($product, $category_nid) {
    $categories = $product->get('field_parent_categories')->getValue();
    return array_search($category_nid, array_column($categories, 'target_id'));
  }

  /**
   * Gets all products in a given category.
   *
   * @return array
   *   An array of products in the category.
   */
  public function getProductsInCategory($category_nid) {
    if ($category_nid != NULL) {
      return $this->productManager->getProductsByCategory($category_nid);
    }
  }

  /**
   * Assigns a product to the given category.
   *
   * @return Drupal\node\Entity\Node
   *   The updated product.
   */
  public function assignToCategory($product, $category) {
    if ($this->getCategoryIndex($product, $category->id()) === FALSE) {
      return $this->productManager->addCategory($product, $category->id());
    }
    else {
      return $product;
    }
  }

  /**
   * Assigns product to the given category, by category node id.
   */
  public function assignToCategoryId($product, $category_nid) {
    if ($this->getCategoryIndex($product, $category_nid) === FALSE) {
      return $this->productManager->addCategory($product, $category_nid);
    }
    else {
      return $product;
    }
  }

  /**
   * Removes the given category from the product, if present.
   *
   * @return Drupal\node\Entity\Node
   *   The updated product.
   */
  public function removeCategory($product, $category) {
    $index = $this->getCategoryIndex($product, $category->id());
    if ($index !== FALSE) {
      return $this->productManager->removeCategory($product, $index);
    }
    else {
      return $product;
    }
  }

  /**
   * Removes the given category id from the product, if present.
   *
   * @return Drupal\node\Entity\Node
   *   The updated product.
   */
  public function removeCategoryId($product, $category_nid) {
    $index = $this->getCategoryIndex($product, $category_nid);
    if ($index !== FALSE) {
      return $this->productManager->removeCategory($product, $index);
    }
    else {
      return $product;
    }
  }

  /**
   * Publish all ep products.
   */
  public function publishProducts() {
    $this->productManager->publish();
  }

}
