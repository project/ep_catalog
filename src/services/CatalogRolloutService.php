<?php

namespace Drupal\ep_catalog\services;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\ep_catalog\Rollout\CatalogRolloutInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Menu\MenuLinkManager;
use Drupal\Core\Path\AliasStorage;

/**
 * Responsible for publishing imported products and populating main navigation.
 */
class CatalogRolloutService implements CatalogRolloutInterface {
  use StringTranslationTrait;

  /**
   * Category service.
   *
   * @var Drupal\ep_catalog\services\CategoryService
   */
  protected $categoryService;

  /**
   * Product service.
   *
   * @var Drupal\ep_catalog\services\ProductService
   */
  protected $productService;

  /**
   * Menu link manager. Used to create menu links for imported EP categories.
   *
   * @var Drupal\Core\Menu\MenuLinkManager
   */
  private $menuLinkManager;

  /**
   * Alias storage system. Used to create aliases for EP categories.
   *
   * @var Drupal\Core\Path\AliasStorage
   */
  private $aliasStorage;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * Constructor.
   */
  public function __construct(
    CategoryService $cat_svc,
    ProductService $prod_svc,
    MenuLinkManager $menuLinkManager,
    AliasStorage $aliasStorage,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $loggerFactory
  ) {
    $this->categoryService = $cat_svc;
    $this->productService = $prod_svc;
    $this->menuLinkManager = $menuLinkManager;
    $this->aliasStorage = $aliasStorage;
    $this->stringTranslation = $string_translation;
    $this->logger = $loggerFactory->get('CatalogRolloutService');
  }

  /**
   * Rollout the catalog.
   */
  public function executeRollout($root_category_code) {
    if (preg_match('/^DRUPAL_.*_ROOT$/', $root_category_code)) {
      if (!$this->createMenuLinks($root_category_code)) {
        return;
      }
    }
    else {
      $msg = 'Category code is not for root category, expected: DRUPAL_<SCOPE>_ROOT';
      drupal_set_message($msg, 'error');
      $this->logger->error($msg);
      return;
    }
    $this->publishProducts();
    drupal_set_message("Catalog with $root_category_code rolled out successfully.");
  }

  /**
   * Retrieve child categories from root category and create menu links.
   */
  protected function createMenuLinks($root_category_code) {
    $this->menuLinkManager->deleteLinksInMenu('main');

    $root_category = $this->categoryService->getCategoryByCode($root_category_code);
    if ($root_category === NULL) {
      $msg = 'Cannot find root category with the code: ' . $root_category_code;
      drupal_set_message($msg, 'error');
      $this->logger->error($msg);
      return;
    }

    $top_category_ids = $root_category->get('field_child_categories')->getValue();
    if ($top_category_ids === NULL) {
      $msg = 'No child categories for root category: ' . $root_category_code;
      drupal_set_message($msg, 'error');
      $this->logger->error($msg);
      return;
    }

    try {
      foreach ($top_category_ids as $category_id) {
        $this->createMenuLink($category_id['target_id'], NULL);
      }
    }
    catch (\DomainException $e) {
      $msg = "Error rolling out category with code $root_category_code.
            Underlying error: \"" . $e->getMessage() . "\"";
      drupal_set_message($msg, 'error');
      drupal_set_message(
        $this->t("Did you try to roll out an old catalog? Please re-run the importer and try again"),
        'error'
      );
      $this->logger->error($msg);
      return;
    }

    return TRUE;
  }

  /**
   * Create menu links for a category and its children.
   */
  protected function createMenuLink($category_id, $parent_id) {
    $category = $this->categoryService->getCategory($category_id);
    if ($category === NULL) {
      throw new \DomainException("Cannot find category with id: $category_id");
    }

    $alias = '/category/' . $category_id;
    $this->createAlias($alias, $category_id);
    $url = 'internal:' . $alias;

    $menu_link = $parent_id === NULL ? $this->createParentLink($category->field_category_name->value, $url)
            : $this->createChildLink($category->field_category_name->value, $parent_id, $url);

    $child_category_ids = $category->get('field_child_categories')->getValue();
    if ($child_category_ids !== NULL) {
      foreach ($child_category_ids as $child_id) {
        $this->createMenuLink($child_id['target_id'], $menu_link->getPluginId());
      }
    }
  }

  /**
   * Creates a menu link for a parent category.
   *
   * @return Drupal\menu_link_content\Plugin\Menu
   *   Newly created menu link.
   */
  protected function createParentLink($category_name, $uri) {
    $menu_link = MenuLinkContent::create([
      'title' => $category_name,
      'menu_name' => 'main',
      'link' => ['uri' => $uri],
      'enabled' => TRUE,
    ]);
    $menu_link->save();
    $this->logger->info('parent link created: ' . $uri);

    return $menu_link;
  }

  /**
   * Creates a menu link for a child category.
   *
   * @return Drupal\menu_link_content\Plugin\Menu
   *   Newly created menu link.
   */
  protected function createChildLink($category_name, $parent_id, $uri) {
    $menu_link = MenuLinkContent::create([
      'title' => $category_name,
      'menu_name' => 'main',
      'link' => ['uri' => $uri],
      'parent' => $parent_id,
      'enabled' => TRUE,
    ]);
    $menu_link->save();
    $this->logger->info('child link created: ' . $uri);

    return $menu_link;
  }

  /**
   * Creates a path alias from a menu link to a category.
   */
  protected function createAlias($alias, $category_id) {
    $alias_svc = $this->aliasStorage;

    if ($alias_svc->lookupPathSource($alias, NULL)) {
      $alias_svc->delete(['alias' => $alias]);
    }

    $alias_svc->save('/node/' . $category_id, $alias);
    $this->logger->info('#createAlias: alias created: ' . $alias);
  }

  /**
   * Publish unpublished ep products.
   */
  protected function publishProducts() {
    $this->productService->publishProducts();
  }

}
