<?php

namespace Drupal\ep_catalog\Rollout;

/**
 * Interface for a catalog rollout.
 */
interface CatalogRolloutInterface {

  /**
   * Run the rollout.
   *
   * @var string root_category_code
   *   Represents the category code of the root EP category. Should have form
   *   DRUPAL_<ep_category>_ROOT.
   */
  public function executeRollout($root_category_code);

}
