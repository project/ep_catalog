<?php

namespace Drupal\ep_catalog\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\ep_catalog\Importer\ProductImporterInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\ep_catalog\Importer\ProductImporterExecutor;

/**
 * ProductImporterForm.  Entrypoint into the importer.
 */
class ProductImporterForm extends FormBase {
  /**
   * The import executor.
   *
   * @var importExecutor
   */
  protected $importExecutor;

  /**
   * Configuration object.
   *
   * @var config
   */
  protected $config;

  /**
   * Constructor.
   */
  public function __construct(
    ProductImporterInterface $product_importer,
    ConfigFactory $config_factory
  ) {
    $this->importExecutor = new ProductImporterExecutor($product_importer);
    $this->config = $config_factory->getEditable('ep_catalog.settings');
  }

  /**
   * Create an instance from a service container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ep_catalog.api_crawler_importer'),
      $container->get('config.factory')
    );
  }

  /**
   * Returns form ID.
   *
   * @{inheritdoc}
   */
  public function getFormId() {
    return 'ep_catalog.importer_form';
  }

  /**
   * Validate the form.  If we have a username, require a password.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $username = $form_state->getValue('username');
    $password = $form_state->getValue('password');

    if (!empty($username) && empty($password)) {
      $form_state->setErrorByName('password', $this->t('Must have password if username is supplied'));
    }
  }

  /**
   * Submit the form and kick off the importer.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $endpoint = $form_state->getValue('endpoint');
    $scope = $form_state->getValue('scope');
    $username = $form_state->getValue('username');
    $password = $form_state->getValue('password');

    $this->config->set('ep_endpoint', $endpoint);
    $this->config->set('ep_scope', $scope);
    $this->config->set('username', $username);
    $this->config->set('password', $password);

    $this->config->save();

    $this->importExecutor->executeImport();
  }

  /**
   * Build the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex URL'),
      '#default_value' => $this->config->get('ep_endpoint'),
      '#required' => TRUE,
    ];

    $form['scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Store Scope'),
      '#default_value' => $this->config->get('ep_scope'),
      '#required' => TRUE,
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex User'),
      '#default_value' => $this->config->get('username'),
      '#required' => FALSE,
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Cortex Password'),
      '#default_value' => $this->config->get('username'),
      '#required' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Products'),
    ];

    return $form;
  }

}
