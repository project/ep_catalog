<?php

namespace Drupal\ep_catalog\product;

use Drupal\Core\Database\Connection;

/**
 * EntityMapper manages the nid => sku mapping in the database.
 */
class ProductEntityMapper {
  protected $dbConnection;

  /**
   * Constructor.
   */
  public function __construct(Connection $conn) {
    $this->dbConnection = $conn;
  }

  /**
   * Creates a mapping between an EP SKU and a Drupal Node ID.
   *
   * @var string sku
   *   EP SKU.
   * @var string nid
   *   Drupal Node ID.
   */
  public function createMapping($sku, $nid) {
    $this->dbConnection
      ->insert('ep_product_mapping')
      ->fields([
        'sku' => $sku,
        'nid' => $nid,
      ])->execute();
  }

  /**
   * Returns the EP SKU associated with a Drupal Node ID, or NULL if no mapping.
   *
   * @var string nid
   *   Drupal Node ID to check SKU for.
   *
   * @return string|null
   *   EP SKU that is mapped to the given Drupal Node ID, or null.
   */
  public function getSku($nid) {
    $result = $this->dbConnection
      ->query('select sku from ep_product_mapping where nid = :nid', [':nid' => $nid])
      ->fetchAssoc('sku');

    if ($result) {
      return $result['sku'];
    }
    return NULL;
  }

  /**
   * Returns the Drupal Node ID mapped to an EP SKU, or NULL if none mapped.
   *
   * @var string sku
   *   EP SKU to search for.
   *
   * @return string|null
   *   Node ID that is mapped to the given SKU, or null.
   */
  public function getNid($sku) {
    $result = $this->dbConnection
      ->query('select nid from ep_product_mapping where sku = :sku', [':sku' => $sku])
      ->fetchAssoc('nid');

    if ($result) {
      return $result['nid'];
    }
    return NULL;
  }

  /**
   * Returns all Drupal Node IDs that are mapped to EP SKUs.
   *
   * @return array
   *   Array of all Drupal Node IDs with EP SKU mappings.
   */
  public function getAllNids() {
    $result = $this->dbConnection
      ->query('select nid from ep_product_mapping;')
      ->fetchAll();

    return ($result) ? $result : [];
  }

  /**
   * Deletes a mapping between an EP SKU and the provided Drupal Node ID.
   *
   * @var string product_nid
   *   Drupal Node ID to delete from the mapping table.
   */
  public function deleteMapping($product_nid) {
    $this->dbConnection
      ->delete('ep_product_mapping')
      ->condition('nid', $product_nid)
      ->execute();
  }

  /**
   * Deletes all mappings between EP SKUs and Drupal Node IDs.
   */
  public function clearMappings() {
    $this->dbConnection->query('delete from ep_product_mapping;')->execute();
  }

}
