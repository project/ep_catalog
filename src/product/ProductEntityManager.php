<?php

namespace Drupal\ep_catalog\product;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;

/**
 * ProductEntityManager manages product nodes in Drupal.
 */
class ProductEntityManager {
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManager $type_man) {
    // TODO: this? Node::*?
    $this->entityTypeManager = $type_man;
  }

  /**
   * Create a product given an array of values.
   */
  public function create(array $vals) {
    $node = Node::create($vals);
    $node->setPublished(TRUE);
    $node->save();

    return $node;
  }

  /**
   * Delete a product by its nid.
   */
  public function delete($nid) {
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    $node->delete();
  }

  /**
   * Loads a node by the node id.
   *
   * @return Drupal\node\Entity\Node
   *   Node with the matching id.
   */
  public function getProductById($nid) {
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    return $node;
  }

  /**
   * Load all ep products and with matching category id.
   *
   * @return array
   *   An array of EP products.
   */
  public function getProductsByCategory($category_nid) {
    $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'ep_product',
      'field_parent_categories' => $category_nid,
    ]);

    return $nodes;
  }

  /**
   * Adds a category by target_id to the categories field on a product.
   *
   * @return Drupal\node\Entity\Node
   *   The updated product that was passed in.
   */
  public function addCategory($product, $category_nid) {
    $product->field_parent_categories[] = ['target_id' => $category_nid];
    $product->save();

    return $product;
  }

  /**
   * Removes a category enitity reference from the product's categories field.
   */
  public function removeCategory($product, $index) {
    $product->get('field_parent_categories')->removeItem($index);
    $product->save();

    return $product;
  }

  /**
   * Publishes all ep product nodes.
   */
  public function publish() {
    $products = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'ep_product',
    ]);

    foreach ($products as $product) {
      $product->setPublished(TRUE);
      $product->save();
    }
  }

}
