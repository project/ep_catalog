<?php

namespace Drupal\ep_catalog\Importer;

/**
 * Interface for a product importer.
 */
interface ProductImporterInterface {

  /**
   * Setup logic.  Returns true if success, false otherwise.
   */
  public function setup();

  /**
   * Get an array containing batches for processing.
   */
  public function getBatches();

  /**
   * Process a batch item.
   */
  public function processBatch($batch);

}
