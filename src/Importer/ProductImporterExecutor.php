<?php

namespace Drupal\ep_catalog\Importer;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\ep_catalog\services\ApiCrawlerImporter;

/**
 * Executes import jobs.
 */
class ProductImporterExecutor {
  protected $importer;

  /**
   * Implements the magic __sleep() method.
   *
   * @return array
   *   Serialization array.
   */
  public function __sleep() {
    return [];
  }

  /**
   * Implements the magic __wakeup() method.
   */
  public function __wakeup() {
    $this->importer = \Drupal::service('ep_catalog.api_crawler_importer');
  }

  /**
   * Constructor.
   */
  public function __construct(ApiCrawlerImporter $imp) {
    $this->importer = $imp;
  }

  /**
   * Called on create.
   *
   * @param Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Symfony DI Container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ep_catalog.api_crawler_importer')
    );
  }

  /**
   * Executes import.
   */
  public function executeImport() {
    if (!$this->importer->setup()) {
      return;
    }

    $categories = $this->importer->getBatches();
    $operations = [];

    foreach ($categories as $category) {
      $operations[] = [
        ['Drupal\ep_catalog\Importer\ProductImporterExecutor', 'executeBatch'],
        [$category],
      ];
    }

    $batch = [
      'title' => 'Importing Products from ElasticPath',
      'operations' => $operations,
    ];

    // Setting up batch to execute.
    batch_set($batch);
  }

  /**
   * Executes a batch for the importer.
   *
   * @param array $batch
   *   Data of batch to process.
   */
  public static function executeBatch(array $batch) {
    $importer = \Drupal::service('ep_catalog.api_crawler_importer');
    $importer->processBatch($batch);

    $rollout_operations[] = [
      ['Drupal\ep_catalog\Importer\ProductImporterExecutor', 'executeRollout'],
      [$importer->getRootCategoryCode()],
    ];

    $rollout_batch = [
      'title' => 'Rolling out catalog',
      'operations' => $rollout_operations,
      'finished' => [
        'Drupal\ep_catalog\Importer\ProductImporterExecutor',
        'onBatchComplete',
      ],
    ];

    batch_set($rollout_batch);
  }

  /**
   * Executes the catalog rollout to create menus and link aliases.
   *
   * @param string $root_code
   *   Root category code.
   */
  public static function executeRollout($root_code) {
    $rollout = \Drupal::service('ep_catalog.catalog_rollout_svc');
    $rollout->executeRollout($root_code);
  }

  /**
   * Called when batch is complete.
   *
   * @param object $success
   *   N/A.
   * @param object $results
   *   Results of batch prcessing.
   * @param array $operations
   *   List of operations completed.
   */
  public static function onBatchComplete($success, $results, array $operations) {
    drupal_set_message(t('Product import complete.'), 'status');
  }

}
