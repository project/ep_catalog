<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test.
 *
 * @group ep_catalog
 */
class EpCatalogSyncKernelTestBase extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'field',
    'text',
    'filter',
    'node',
    'link',
    'menu_ui',
    'menu_link_content',
    'ep_catalog',
    'ep_catalog',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('menu_link_content');
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['node', 'field', 'ep_catalog']);
  }

}
