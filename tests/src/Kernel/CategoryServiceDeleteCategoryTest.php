<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\node\Entity\Node;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceDeleteCategoryTest extends CategoryServiceKernelTestBase {

  /**
   * Tests that the function deletes the category.
   */
  public function testDeletesCategory() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $cat = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $id = $cat->id();
    $this->assertTrue(Node::load($id) != NULL);

    $category_service->deleteCategory($cat);
    $this->assertTrue(Node::load($id) == NULL);
  }

}
