<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceKernelTestBase extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'field',
    'text',
    'filter',
    'node',
    'menu_ui',
    'ep_catalog',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['node', 'field', 'ep_catalog']);
  }

}
