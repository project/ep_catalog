<?php

namespace Drupal\Tests\ep_catalog\Kernel;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceRemoveChildTest extends CategoryServiceKernelTestBase {

  /**
   * Tests that the function removes the child from the parent.
   */
  public function testRemovesChildFromParent() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $child_1 = $category_service->createCategory([
      'field_category_code' => 'child_1',
      'field_category_name' => 'Child 1',
    ]);

    $child_2 = $category_service->createCategory([
      'field_category_code' => 'child_2',
      'field_category_name' => 'Child 2',
    ]);

    $child_3 = $category_service->createCategory([
      'field_category_code' => 'child_3',
      'field_category_name' => 'Child 3',
    ]);

    $category_service->addChild($parent, $child_1);
    $category_service->addChild($parent, $child_2);
    $category_service->addChild($parent, $child_3);

    $children = $parent->get('field_child_categories')->getValue();
    $this->assertTrue(in_array(['target_id' => $child_1->id()], $children));
    $this->assertTrue(in_array(['target_id' => $child_2->id()], $children));
    $this->assertTrue(in_array(['target_id' => $child_3->id()], $children));

    $category_service->removeChild($parent, $child_1);
    $children = $parent->get('field_child_categories')->getValue();
    $this->assertTrue(!in_array(['target_id' => $child_1->id()], $children));
    $this->assertTrue(in_array(['target_id' => $child_2->id()], $children));
    $this->assertTrue(in_array(['target_id' => $child_3->id()], $children));

    $category_service->removeChild($parent, $child_2);
    $children = $parent->get('field_child_categories')->getValue();
    $this->assertTrue(!in_array(['target_id' => $child_1->id()], $children));
    $this->assertTrue(!in_array(['target_id' => $child_2->id()], $children));
    $this->assertTrue(in_array(['target_id' => $child_3->id()], $children));

    $category_service->removeChild($parent, $child_3);
    $children = $parent->get('field_child_categories')->getValue();
    $this->assertTrue(!in_array(['target_id' => $child_1->id()], $children));
    $this->assertTrue(!in_array(['target_id' => $child_2->id()], $children));
    $this->assertTrue(!in_array(['target_id' => $child_3->id()], $children));
  }

  /**
   * Tests that the function returns the updated parent node.
   */
  public function testReturnsUpdatedParent() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $child_1 = $category_service->createCategory([
      'field_category_code' => 'child_1',
      'field_category_name' => 'Child 1',
    ]);

    $category_service->addChild($parent, $child_1);
    $child_arr = $parent->get('field_child_categories')->getValue();
    $this->assertTrue(count($child_arr) == 1);

    $result = $category_service->removeChild($parent, $child_1);
    $child_arr = $result->get('field_child_categories')->getValue();
    $this->assertTrue($result == $parent);
    $this->assertTrue(count($child_arr) == 0);
  }

}
