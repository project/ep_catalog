<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\node\Entity\Node;

/**
 * @covers CatalogRolloutService
 *
 * @group ep_catalog
 */
class CategoryServiceCreateCategoryTest extends CategoryServiceKernelTestBase {

  /**
   * Tests that the function returns an EP Category node.
   */
  public function testReturnsAnEpCategoryNode() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $output_category = $category_service->createCategory([
      'field_category_code' => 'test_category',
      'field_category_name' => 'Test Category',
    ]);

    $this->assertTrue($output_category instanceof Node);
    $this->assertTrue($output_category->getType() == 'ep_category');
  }

  /**
   * Tests that the function creates a category node.
   */
  public function testCreatesCategoryNode() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $id = $category_service->createCategory([
      'field_category_code' => 'test_category',
      'field_category_name' => 'Test Category',
    ])->id();

    $result = Node::load($id);
    $this->assertTrue($result instanceof Node);
    $this->assertTrue($result->getType() == 'ep_category');
  }

  /**
   * Tests that function throws exception on duplicate category code.
   */
  public function testThrowsExceptionOnDuplicateCategoryCode() {
    $this->expectException(\DomainException::class);
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $category_service->createCategory([
      'field_category_code' => 'test_category',
      'field_category_name' => 'Test Category 1',
    ]);

    $category_service->createCategory([
      'field_category_code' => 'test_category',
      'field_category_name' => 'Test Category 2',
    ]);
  }

}
