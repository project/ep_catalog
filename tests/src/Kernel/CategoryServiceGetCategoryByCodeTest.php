<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\node\Entity\Node;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceGetCategoryByCodeTest extends CategoryServiceKernelTestBase {

  /**
   * Tests that the function returns an EP category node if one exists.
   */
  public function testReturnsEpCategoryNodeIfExists() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $category = $category_service->createCategory([
      'field_category_code' => 'test_category',
      'field_category_name' => 'Test Category 1',
    ]);
    $id = $category->id();

    $output = $category_service->getCategoryByCode('test_category');

    $this->assertTrue($output instanceof Node);
    $this->assertTrue($output->getType() == 'ep_category');
    $this->assertTrue($output->id() == $id);
  }

  /**
   * Tests that the function returns NULL if no match found.
   */
  public function testReturnsNullIfNonexistant() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $output = $category_service->getCategoryByCode('test_category');

    $this->assertTrue($output == NULL);
  }

}
