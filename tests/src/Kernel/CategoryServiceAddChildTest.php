<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\node\Entity\Node;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceAddChildTest extends CategoryServiceKernelTestBase {

  /**
   * Tests that the function adds child to the parent's child categories field.
   */
  public function testAddsChildToParentChildCategoriesField() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $child = $category_service->createCategory([
      'field_category_code' => 'child_category',
      'field_category_name' => 'Child',
    ]);

    $parent_id = $parent->id();
    $child_id = $child->id();
    $category_service->addChild($parent, $child);

    Node::load($parent_id);
    $result_child_cats = $parent->get('field_child_categories')->getValue();

    $this->assertTrue(is_array($result_child_cats));
    $this->assertTrue(in_array(['target_id' => $child_id], $result_child_cats));
  }

  /**
   * Tests that the function returns the parent node.
   */
  public function testReturnsParentNode() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $child = $category_service->createCategory([
      'field_category_code' => 'child_category',
      'field_category_name' => 'Child',
    ]);

    $result = $category_service->addChild($parent, $child);
    $this->assertTrue($result == $parent);
  }

  /**
   * Tests that the function errors if the parent is null.
   */
  public function testErrorsOnNullParent() {
    $this->expectException(\TypeError::class);
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = NULL;

    $child = $category_service->createCategory([
      'field_category_code' => 'child_category',
      'field_category_name' => 'Child',
    ]);

    $category_service->addChild($parent, $child);
  }

  /**
   * Tests that the function errors if child is null.
   */
  public function testErrorsOnNullChild() {
    $this->expectException(\TypeError::class);
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $child = NULL;

    $category_service->addChild($parent, $child);
  }

}
