<?php

namespace Drupal\Tests\ep_catalog\Kernel;

/**
 * Kernel tests for CatalogRolloutService.
 *
 * @cover CatalogRolloutService.
 *
 * @group ep_catalog
 */
class CatalogRolloutKernelTest extends EpCatalogSyncKernelTestBase {

  /**
   * Tests creating aliases.
   */
  public function testCreateAlias() {
    $category_service = \Drupal::service('ep_catalog.category_svc');
    $rollout_service = \Drupal::service('ep_catalog.catalog_rollout_svc');
    $alias_svc = \Drupal::service('path.alias_storage');

    $category = $category_service->createCategory([
      'field_category_code' => 'TEST_CATEGORY',
      'field_category_name' => 'Test Category',
    ]);

    $category_id = $category->id();
    $alias = '/category/' . $category_id;

    $class = new \ReflectionClass($rollout_service);
    $method = $class->getMethod('createAlias');
    $method->setAccessible(TRUE);
    $method->invokeArgs($rollout_service, [$alias, $category_id]);

    $this->assertTrue($alias_svc->lookupPathSource($alias, NULL));
  }

  /**
   * Tests creating parent links.
   */
  public function testCreateParentLink() {
    $category_service = \Drupal::service('ep_catalog.category_svc');
    $rollout_service = \Drupal::service('ep_catalog.catalog_rollout_svc');

    $category = $category_service->createCategory([
      'field_category_code' => 'TEST_CATEGORY',
      'field_category_name' => 'Test Category',
    ]);

    $category_id = $category->id();
    $alias = '/category/' . $category_id;
    $url = 'internal:' . $alias;

    $class = new \ReflectionClass($rollout_service);
    $createAlias = $class->getMethod('createAlias');
    $createAlias->setAccessible(TRUE);
    $createAlias->invokeArgs($rollout_service, [$alias, $category_id]);

    $createParent = $class->getMethod('createParentLink');
    $createParent->setAccessible(TRUE);
    $menu_link = $createParent->invokeArgs($rollout_service, [$category->field_category_name->value, $url]);

    $loaded_link = \Drupal::service('plugin.manager.menu.link')->getDefinition($menu_link->getPluginId());

    $this->assertTrue($menu_link->getPluginId() === $loaded_link['id']);
  }

  /**
   * Tests creating child links.
   */
  public function testCreateChildLink() {
    $category_service = \Drupal::service('ep_catalog.category_svc');
    $rollout_service = \Drupal::service('ep_catalog.catalog_rollout_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'PARENT_CATEGORY',
      'field_category_name' => 'Parent Category',
    ]);

    $child = $category_service->createCategory([
      'field_category_code' => 'CHILD_CATEGORY',
      'field_category_name' => 'Child Category',
    ]);

    $parent_id = $parent->id();
    $parent_alias = '/category/' . $parent_id;
    $parent_url = 'internal:' . $parent_alias;

    $child_id = $child->id();
    $child_alias = '/category/' . $child_id;
    $child_url = 'internal:' . $child_alias;

    $class = new \ReflectionClass($rollout_service);
    $createAlias = $class->getMethod('createAlias');
    $createAlias->setAccessible(TRUE);
    $createAlias->invokeArgs($rollout_service, [$parent_alias, $parent_id]);
    $createAlias->invokeArgs($rollout_service, [$child_alias, $child_id]);

    $createParent = $class->getMethod('createParentLink');
    $createParent->setAccessible(TRUE);
    $parent_link = $createParent->invokeArgs($rollout_service, [$parent->field_category_name->value, $parent_url]);

    $createChild = $class->getMethod('createChildLink');
    $createChild->setAccessible(TRUE);
    $child_link = $createChild->invokeArgs(
      $rollout_service,
      [
        $child->field_category_name->value,
        $parent_link->getPluginId(),
        $child_url,
      ]
    );

    $loaded_link = \Drupal::service('plugin.manager.menu.link')->getDefinition($child_link->getPluginId());

    $this->assertTrue($loaded_link['parent'] === $parent_link->getPluginId());
  }

  /**
   * Tests creating menu links.
   */
  public function testCreateMenuLink() {
    $category_service = \Drupal::service('ep_catalog.category_svc');
    $rollout_service = \Drupal::service('ep_catalog.catalog_rollout_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'PARENT_CATEGORY',
      'field_category_name' => 'Parent Category',
    ]);

    $child = $category_service->createCategory([
      'field_category_code' => 'CHILD_CATEGORY',
      'field_category_name' => 'Child Category',
    ]);

    $category_service->addChild($parent, $child);

    $class = new \ReflectionClass($rollout_service);
    $createMenuLink = $class->getMethod('createMenuLink');
    $createMenuLink->setAccessible(TRUE);
    $createMenuLink->invokeArgs($rollout_service, [$parent->id(), NULL]);

    $menu_tree = \Drupal::menuTree();
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters('main');
    $tree = $menu_tree->load('main', $parameters);
    $keys = array_keys($tree);

    $this->assertTrue($tree[$keys[0]]->hasChildren);
  }

}
