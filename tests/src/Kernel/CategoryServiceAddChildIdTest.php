<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\node\Entity\Node;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceAddChildIdTest extends CategoryServiceKernelTestBase {

  /**
   * Tests adding a child category to the 'child categories field'.
   */
  public function testAddsChildToParentChildCategoriesField() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $child = $category_service->createCategory([
      'field_category_code' => 'child_category',
      'field_category_name' => 'Child',
    ]);

    $parent_id = $parent->id();
    $child_id = $child->id();
    $category_service->addChildId($parent, $child_id);

    Node::load($parent_id);
    $result_child_cats = $parent->get('field_child_categories')->getValue();

    $this->assertTrue(is_array($result_child_cats));
    $this->assertTrue(in_array(['target_id' => $child_id], $result_child_cats));
  }

  /**
   * Tests that the function returns the parent node.
   */
  public function testReturnsParentNode() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $child = $category_service->createCategory([
      'field_category_code' => 'child_category',
      'field_category_name' => 'Child',
    ]);

    $result = $category_service->addChildId($parent, $child->id());
    $this->assertTrue($result == $parent);
  }

  /**
   * Tests that the function throws errors when parent is null.
   */
  public function testErrorsOnNullParent() {
    $this->expectException(\TypeError::class);
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = NULL;

    $child = $category_service->createCategory([
      'field_category_code' => 'child_category',
      'field_category_name' => 'Child',
    ]);

    $category_service->addChildId($parent, $child);
  }

  /**
   * Tests that function throws errors on nonexistant child.
   */
  public function testErrorsOnNonexistantChild() {
    $this->expectException(\InvalidArgumentException::class);
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $this->assertTrue(Node::load('12345') == NULL);

    $category_service->addChildId($parent, '12345');
  }

}
