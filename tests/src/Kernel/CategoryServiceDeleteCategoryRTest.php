<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\node\Entity\Node;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceDeleteCategoryRTest extends CategoryServiceKernelTestBase {

  /**
   * Tests that the function deletes the category.
   */
  public function testDeletesCategory() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $cat = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $id = $cat->id();
    $this->assertTrue(Node::load($id) != NULL);

    $category_service->deleteCategory($cat);
    $this->assertTrue(Node::load($id) == NULL);
  }

  /**
   * Tests that the function deletes all child categories.
   */
  public function testDeletesAllChildCategories() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $parent = $category_service->createCategory([
      'field_category_code' => 'parent_category',
      'field_category_name' => 'Parent',
    ]);

    $parent_id = $parent->id();
    $child_ids = [];
    for ($i = 0; $i < 5; $i++) {
      $child = $category_service->createCategory([
        'field_category_code' => 'child_category_' . $i,
        'field_category_name' => 'Child ' . $i,
      ]);
      $category_service->addChild($parent, $child);
      array_push($child_ids, $child->id());
      $this->assertTrue(Node::load($child->id()) != NULL);
    }

    $category_service->deleteCategoryR($parent);

    $this->assertTrue(Node::load($parent_id) == NULL);
    foreach ($child_ids as $id) {
      $this->assertTrue(Node::load($id) == NULL);
    }
  }

  /**
   * Tests that the funciton deletes grand children.
   */
  public function testDeletesGrandChildren() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $cat_ids    = [];
    $parent     = NULL;
    $last_child = NULL;

    for ($i = 0; $i < 5; $i++) {
      $child = $category_service->createCategory([
        'field_category_code' => 'child_category_' . $i,
        'field_category_name' => 'Child ' . $i,
      ]);

      array_push($cat_ids, $child->id());
      $this->assertTrue(Node::load($child->id()) != NULL);

      if ($last_child) {
        $category_service->addChild($last_child, $child);
      }
      else {
        $parent = $child;
      }

      $last_child = $child;
    }

    $category_service->deleteCategoryR($parent);

    foreach ($cat_ids as $id) {
      $this->assertTrue(Node::load($id) == NULL);
    }
  }

}
