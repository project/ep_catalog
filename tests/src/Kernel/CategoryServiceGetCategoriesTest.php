<?php

namespace Drupal\Tests\ep_catalog\Kernel;

use Drupal\node\Entity\Node;

/**
 * Just a dummy test. This will be removed later.
 *
 * @group ep_catalog
 */
class CategoryServiceGetCategoriesTest extends CategoryServiceKernelTestBase {

  /**
   * Tests that teh function returns an array of EP categories on match.
   */
  public function testReturnsArrayOfEpCategoryNodesOnMatch() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $ids = [];
    $category = $category_service->createCategory([
      'field_category_code' => 'test_category_1',
      'field_category_name' => 'Test Category 1',
    ]);
    array_push($ids, $category->id());

    $category = $category_service->createCategory([
      'field_category_code' => 'test_category_2',
      'field_category_name' => 'Test Category 2',
    ]);
    array_push($ids, $category->id());

    $category_array = $category_service->getCategories($ids);
    $this->assertTrue(is_array($category_array));
    $this->assertTrue(count($category_array) == 2);

    foreach ($category_array as $element) {
      $this->assertTrue($element instanceof Node);
      $this->assertTrue($element->getType() == 'ep_category');
      $this->assertTrue(in_array($element->id(), array_values($ids)));
    }
  }

  /**
   * Tests that the funciton returns an empty array if no matches were found.
   */
  public function testReturnsEmptyArrayIfNoMatches() {
    $category_service = \Drupal::service('ep_catalog.category_svc');

    $ids = [];

    $category_array = $category_service->getCategories($ids);
    $this->assertTrue(is_array($category_array));
    $this->assertTrue(count($category_array) == 0);
  }

}
